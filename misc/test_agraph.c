
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include <r_core.h>
#include <r_anal.h>
#include <r_agraph.h>
#include <r_util.h>
#include "minunit.h"

bool test_graph_to_agraph() {
	RCore *core = r_core_new ();
	r_core_cmd0 (core, "ac A");
	r_core_cmd0 (core, "ac B");
	r_core_cmd0 (core, "ac C");
	r_core_cmd0 (core, "ac D");
	r_core_cmd0 (core, "acb B A");
	r_core_cmd0 (core, "acb C A");
	r_core_cmd0 (core, "acb D B");
	r_core_cmd0 (core, "acb D C");

	RGraph *graph = r_anal_class_get_inheritance_graph (core->anal);
	mu_assert_notnull (graph, "Couldn't create the graph");
	mu_assert_eq (graph->nodes->length, 4, "Wrong node count");

	RAGraph *agraph = create_agraph_from_graph (graph);
	mu_assert_notnull (agraph, "Couldn't create the graph");
	mu_assert_eq (agraph->graph->nodes->length, 4, "Wrong node count");

	RListIter *iter;
	RGraphNode *node;
	int i = 0;
	ls_foreach (agraph->graph->nodes, iter, node) {
		RANode *info = node->data;
		switch (i++) {
		case 0:
			mu_assert_streq (info->title, "A", "Wrong node name");
			mu_assert_eq (node->out_nodes->length, 2, "Wrong node out-nodes");
			{
				RListIter *iter;
				RGraphNode *out_node;
				int i = 0;
				ls_foreach (node->out_nodes, iter, out_node) {
					RANode *info = out_node->data;
					switch (i++) {
					case 0:
						mu_assert_streq (info->title, "B", "Wrong node name");
						break;
					case 1:
						mu_assert_streq (info->title, "C", "Wrong node name");
						break;
					}
				}
			}
			break;
		case 1:
			mu_assert_streq (info->title, "B", "Wrong node name");
			mu_assert_eq (node->out_nodes->length, 1, "Wrong node out-nodes");
			mu_assert_eq (node->in_nodes->length, 1, "Wrong node in-nodes");
			{
				RListIter *iter;
				RGraphNode *out_node;
				int i = 0;
				ls_foreach (node->out_nodes, iter, out_node) {
					RANode *info = out_node->data;
					switch (i++) {
					case 0:
						mu_assert_streq (info->title, "D", "Wrong node name");
						break;
					}
				}
			}
			break;
		case 2:
			mu_assert_streq (info->title, "C", "Wrong node name");
			mu_assert_eq (node->out_nodes->length, 1, "Wrong node out-nodes");
			mu_assert_eq (node->in_nodes->length, 1, "Wrong node in-nodes");
			{
				RListIter *iter;
				RGraphNode *out_node;
				int i = 0;
				ls_foreach (node->out_nodes, iter, out_node) {
					RANode *info = out_node->data;
					switch (i++) {
					case 0:
						mu_assert_streq (info->title, "D", "Wrong node name");
						break;
					}
				}
			}
			break;
		case 3:
			mu_assert_streq (info->title, "D", "Wrong node name");
			mu_assert_eq (node->in_nodes->length, 2, "Wrong node in-nodes");
			break;
		default:
			break;
		}
	}
// run placement and write svg image
	r_core_free (core);
	r_graph_free (graph);
	r_agraph_free (agraph);
	mu_end;
}

int all_tests() {
	mu_run_test (test_graph_to_agraph);
	return tests_passed != tests_run;
}

int main(int argc, char **argv) {
	return all_tests ();
}

/* end. */
