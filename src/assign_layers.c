
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* assign a layer to each node of the graph.
 *
 * It visits the nodes of the graph in the topological sort, so that every time
 * you visit a node, you can be sure that you have already visited all nodes
 * that can lead to that node and thus you can easily compute the layer based
 * on the layer of these "parent" nodes. But that can also be done with dfs.
 */
void assign_layers(const RAGraph * g)
{
    RGraphVisitor layer_vis = {
	NULL, NULL, NULL, NULL, NULL, NULL
    };
    const RGraphNode *gn;
    const RListIter *it;
    RANode *n;
    RList *topological_sort = r_list_new();

    /* why this topological? why not the usual dfs? */
    layer_vis.data = topological_sort;
    layer_vis.finish_node = (RGraphNodeCallback) add_sorted;
    r_graph_dfs(g->graph, &layer_vis);

    graph_foreach_anode(topological_sort, it, gn, n) {
	const RList *innodes = r_graph_innodes(g->graph, gn);
	RListIter *it;
	RGraphNode *prev;
	RANode *preva;

	/* the relative vertical level of this node vertex */
	n->layer = 0;		/* option here to set other start level */
	graph_foreach_anode(innodes, it, prev, preva) {
	    if ((preva->layer + 1) > n->layer) {
		n->layer = (preva->layer + 1);
	    }
	}
    }

    r_list_free(topological_sort);

    return;
}

/* end. */
