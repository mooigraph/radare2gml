
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

static int RM_listcmp(const struct len_pos_t *a, const struct len_pos_t *b)
{
    return (a->pos < b->pos) - (a->pos > b->pos);
}

static int RP_listcmp(const struct len_pos_t *a, const struct len_pos_t *b)
{
    return (a->pos > b->pos) - (a->pos < b->pos);
}

/* see the buchheim theory */
void collect_changes(const RAGraph * g, int l, const RGraphNode * b, int from_up, int s, int e, RList * list, int is_left)
{
    const RGraphNode *vt = g->layers[l].nodes[e - 1];
    const RGraphNode *vtp = g->layers[l].nodes[s];
    struct len_pos_t *cx = NULL;
    int i = 0;

    RListComparator lcmp = is_left ? (RListComparator) RM_listcmp : (RListComparator) RP_listcmp;

    /* this can be done easier. */
    for (i = is_left ? s : e - 1; (is_left && i < e) || (!is_left && i >= s); i = is_left ? i + 1 : i - 1) {
	const RGraphNode *v, *vi = g->layers[l].nodes[i];
	const RANode *av, *avi = get_anode(vi);
	const RList *neigh;
	const RListIter *it;
	int c = 0;

	if (!avi) {
	    continue;
	}
	neigh = from_up ? r_graph_innodes(g->graph, vi)
	    : r_graph_get_neighbours(g->graph, vi);

	graph_foreach_anode(neigh, it, v, av) {
	    if ((is_left && av->x >= avi->x) || (!is_left && av->x <= avi->x)) {
		c++;
	    } else {
		cx = R_NEW(struct len_pos_t);
		c--;
		cx->len = 2;
		cx->pos = av->x;
		if (is_left) {
		    cx->pos += dist_nodes(g, vi, vt);
		} else {
		    cx->pos -= dist_nodes(g, vtp, vi);
		}
		r_list_add_sorted(list, cx, lcmp);
	    }
	}

	cx = R_NEW0(struct len_pos_t);
	cx->len = c;
	cx->pos = avi->x;
	if (is_left) {
	    cx->pos += dist_nodes(g, vi, vt);
	} else {
	    cx->pos -= dist_nodes(g, vtp, vi);
	}
	r_list_add_sorted(list, cx, lcmp);
    }

    if (b) {
	const RANode *ab = get_anode(b);
	cx = R_NEW(struct len_pos_t);

	cx->len = is_left ? INT_MAX : INT_MIN;
	cx->pos = ab->x;
	if (is_left) {
	    cx->pos += dist_nodes(g, b, vt);
	} else {
	    cx->pos -= dist_nodes(g, vtp, b);
	}
	r_list_add_sorted(list, cx, lcmp);

    }

    return;
}

/* end. */
