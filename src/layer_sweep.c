
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* layer number is in i, from_up is 1 if from top-to-bottom */
int layer_sweep(const RGraph * g, const struct layer_t layers[], int maxlayer, int i, int from_up)
{
    RGraphNode *u = NULL;
    RGraphNode *v = NULL;
    const RANode *au = NULL;
    const RANode *av = NULL;
    int n_rows = 0;
    int j = 0;
    int changed = 0;		/* false */
    int len = 0;
    int auidx = 0;
    int avidx = 0;
    int **cross_matrix = NULL;
    RANode *n = NULL;

    /* number of nodes at this level i is always > 0 */
    len = layers[i].n_nodes;

    /* n_rows is set by get_crossing_matrix() number of nodes at level */
    cross_matrix = get_crossing_matrix(g, layers, maxlayer, i, from_up, &n_rows);

    for (j = 0; j < (len - 1); j++) {

	u = layers[i].nodes[j];
	v = layers[i].nodes[j + 1];

	au = get_anode(u);
	av = get_anode(v);

	/* get node positions in this level */
	auidx = au->pos_in_layer;
	avidx = av->pos_in_layer;

	if (cross_matrix[auidx][avidx] > cross_matrix[avidx][auidx]) {
	    /* swap elements */
	    layers[i].nodes[j] = v;
	    layers[i].nodes[j + 1] = u;
	    changed = 1		/* true */
	}
    }

    /* update position in the layer of each node. During the swap of some
     * elements we didn't swap also the pos_in_layer because the cross_matrix
     * is indexed by it, so do it now! */
    for (j = 0; j < layers[i].n_nodes; j++) {
	n = get_anode(layers[i].nodes[j]);
	/* set relative x position of node in this level */
	n->pos_in_layer = j;
    }

    for (j = 0; j < n_rows; j++) {
	R_FREE(cross_matrix[j]);
    }

    R_FREE(cross_matrix);

    return changed;
}

/* end. */
