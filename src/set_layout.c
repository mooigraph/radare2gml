
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

/* 1) transform the graph into a DAG
 * 2) partition the nodes in layers
 * 3) split long edges that traverse multiple layers
 * 4) reorder nodes in each layer to reduce the number of edge crossing
 * 5) assign x and y coordinates to each node
 * 6) restore the original graph, with long edges and cycles */
static void set_layout(RAGraph *g) {
	int i, j, k;

	r_list_free (g->edges);  // why is this?
	g->edges = r_list_new ();

	remove_cycles (g); // clear cycles
	assign_layers (g); // do topological, why? that reverses the drawing
	create_dummy_nodes (g); // span multiple edges
	create_layers (g); // this is?
	minimize_crossings (g); // matrix barycenter

	/* identify row height */
	for (i = 0; i < g->n_layers; i++) {
		int rh = 0; // why this local var?
		int rw = 0;
		for (j = 0; j < g->layers[i].n_nodes; j++) {
			const RANode *n = get_anode (g->layers[i].nodes[j]);
			if (n->h > rh) {
				rh = n->h;
			}
			if (n->w > rw) { // max w in one of the nodes
				rw = n->w;
			}
		}
		g->layers[i].height = rh;
		g->layers[i].width = rw; // huh
	}

	for (i = 0; i < g->n_layers; i++) {
		for (j = 0; j < g->layers[i].n_nodes; j++) {
			RANode *a = (RANode *) g->layers[i].nodes[j]->data;
			if (a->is_dummy) {
				if (g->layout == 0) {
				// vertical layout
					a->h = g->layers[i].height;
				} else {
				// horizontal layout
					a->w = g->layers[i].width;
				}
			}
			a->layer_height = g->layers[i].height;
			a->layer_width = g->layers[i].width;  // max width of one of the nodes in this level, is this oke?
		}
	}

	/* x-coordinate assignment: algorithm based on:
	 * A Fast Layout Algorithm for k-Level Graphs
	 * by C. Buchheim, M. Junger, S. Leipert
	 * george sander does not exist, that is a fake name.
	 */
	place_dummies (g); // see the paper
	place_original (g); // see the paper

  // why is this not a seperate routine? place_levels()


	/* IDEA: need to put this hack because of the way algorithm is implemented.
	 * I think backedges should be restored to their original state instead of
	 * converting them to longedges and adding dummy nodes. */
	const RListIter *it;
	const RGraphEdge *e;
	r_list_foreach (g->back_edges, it, e) {
		RANode *from = e->from? get_anode (e->from): NULL;
		RANode *to = e->to? get_anode (e->to): NULL;
		// can it happen there is no from/to ?
		fix_back_edge_dummy_nodes (g, from, to);
		r_agraph_del_edge (g, to, from);
		r_agraph_add_edge_at (g, from, to, e->nth);
	}

	switch (g->layout) {
	default: // should not happen ?
	case 0: // vertical layout
		/* horizontal finalize x coordinate */
		for (i = 0; i < g->n_layers; i++) {
			for (j = 0; j < g->layers[i].n_nodes; j++) {
				RANode *n = get_anode (g->layers[i].nodes[j]);
				if (n) { // needed ?
					n->x -= n->w / 2;
					if (g->is_tiny) { // this is ?
						n->x /= 8; // why 8, why not a var
					}
				}
			}
		}

		set_layer_gap (g); // this is ?

		/* vertical align */
		for (i = 0; i < g->n_layers; i++) {
			int tmp_y = 0;
			tmp_y = g->layers[0].gap; //TODO: XXX: set properly -- why
			for (k = 1; k <= i; k++) {
				tmp_y += g->layers[k-1].height + g->layers[k].gap + 3; //XXX: should be 4? -- why?
			}
			if (g->is_tiny) { // this is?
				tmp_y = i;
			}
			for (j = 0; j < g->layers[i].n_nodes; j++) {
				RANode *n = get_anode (g->layers[i].nodes[j]);
				if (n) { // why ? shouldnothappen cannot happen ?
					n->y = tmp_y;
				}
			}
		}
		break;
	/* experimental */
	case 1: // horizontal layout
		/* vertical y coordinate */
		for (i = 0; i < g->n_layers; i++) {
			for (j = 0; j < g->layers[i].n_nodes; j++) {
				RANode *n = get_anode (g->layers[i].nodes[j]);
				n->y = 1;  // if n ?
				for (k = 0; k < j; k++) {
					RANode *m = get_anode (g->layers[i].nodes[k]);
					n->y -= (m->h + VERTICAL_NODE_SPACING); // make VERTICAL_NODE_SPACING into a variable
				}
			}
		}

		set_layer_gap (g); // this is ?

		/* horizontal align */
		for (i = 0; i < g->n_layers; i++) {
			int xval = 1 + g->layers[0].gap + 1; // why +1 ? why not a var?
			for (k = 1; k <= i; k++) {
// max width of one of the nodes in this level sure?
				xval += g->layers[k-1].width + g->layers[k].gap + 3; // + +3 ?
			}
			for (j = 0; j < g->layers[i].n_nodes; j++) {
				RANode *n = get_anode (g->layers[i].nodes[j]); // why local RANonde ? everywhere
				n->x = xval; // if n
			}
		}
		break;
	}

	backedge_info (g); // this is ?

	/* free all temporary structures used during layout */
	for (i = 0; i < g->n_layers; i++) {
		free (g->layers[i].nodes);
	}

	free (g->layers);
	r_list_free (g->long_edges);
	r_list_free (g->back_edges);
	r_cons_break_pop (); // console routines

	return;
}

/* end. */
