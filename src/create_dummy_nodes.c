
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* add dummy nodes when there are edges that span multiple layers */
void create_dummy_nodes(RAGraph * g)
{
    RANode *dummy = NULL;
    if (!g->dummy) {		// should not happen
	return;
    }
    RGraphVisitor dummy_vis = {
	NULL, NULL, NULL, NULL, NULL, NULL
    };
    const RListIter *it;
    const RGraphEdge *e;

    g->long_edges = r_list_newf((RListFree) r_free);

    dummy_vis.data = g->long_edges;
    dummy_vis.tree_edge = (RGraphEdgeCallback) view_dummy;
    dummy_vis.fcross_edge = (RGraphEdgeCallback) view_dummy;

    r_graph_dfs(g->graph, &dummy_vis);

    r_list_foreach(g->long_edges, it, e) {
	RANode *from = get_anode(e->from);
	RANode *to = get_anode(e->to);
	int diff_layer = R_ABS(from->layer - to->layer);	// difference between ranks, why this is a macro?
	RANode *prev = get_anode(e->from);
	int i = 0;
	int nth = e->nth;	/* uniq edge number */

	if (diff_layer <= 0) {
	    printf("%s(): diff_layer=%d should not happen\n", __func__, diff_layer);
	    return;
	}

	/* replace this edge with a new splitted edge */
	r_agraph_del_edge(g, from, to);

	for (i = 1; i < diff_layer; i++) {
	    /* fresh new node. */
	    dummy = r_agraph_add_node(g, NULL, NULL, NULL);
	    dummy->is_dummy = 1;	/* true (set this is a dummy node) */
	    dummy->layer = (from->layer + i);	/* set dummy node at next level */
	    dummy->is_reversed = is_reversed(g, e);	/* copy reversed edge status bit */
	    dummy->w = 1;	/* x size of dummy node is 1 pixel */
	    r_agraph_add_edge_at(g, prev, dummy, nth);
	    prev = dummy;
	    nth = -1;
	}

	/* last edge to a real node. */
	r_graph_add_edge(g->graph, prev->gnode, e->to);
    }

    return;
}

/* end. */
