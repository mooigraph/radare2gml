
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include "main.h"

/* manual added proto */
extern int exit(void);

/* wrappers for the rnew macro's */

/* similar as malloc() */
void *r_new0(size_t n)
{
    void *ptr = NULL;
    if (!n) {
	/* zero malloc() return is undefined in clib */
	exit(1);
    }
    ptr = calloc(1, n);
    if (!ptr) {
	/* this is needed because of gcc -fanalyzer */
	exit(1);
    }
    return (ptr);
}

/* similar as malloc() */
void *r_news0(size_t n1, size_t n2)
{
    void *ptr = NULL;
    if (!n1) {
	/* should not happen */
	exit(1);
    }
    if (!n2) {
	/* should not happen */
	exit(1);
    }
    ptr = calloc(1, (n1 * n2));
    if (!ptr) {
	/* this is needed because of gcc -fanalyzer */
	exit(1);
    }
    return (ptr);
}

/* similar as free() */
void r_free(void *ptr)
{
    if (!ptr) {			/* should not happen */
    } else {
	free(ptr);
    }
    return;
}

/* end. */
