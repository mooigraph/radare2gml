
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#define HORIZONTAL_NODE_SPACING 4
#define VERTICAL_NODE_SPACING 2

/* how many max layer sweeps */
#define MAX_SWEEP_CHANGES (4096*16)	/* orig: 4096 */

/* wrappers to rnew.c */
#define R_NEW(x) r_new0(sizeof(x))
#define R_NEW0(x) r_new0(sizeof(x))
#define R_NEWS0(a,b) r_news0(sizeof(a)*b)
#define R_FREE(x) r_free(x)

/* rnew.c */
extern void *r_new0(size_t n);
extern void *r_news0(size_t n1, size_t n2);
extern void r_free(void *ptr);

/* end. */
