
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#ifdef __cplusplus
#error "Sorry, this is not possible"
#endif

#if __WINDOWS__
#error "Sorry, install real GNU GPL Free Software GNU/Linux see fsf.org or debian.org"
#endif

#ifdef WIN32
#error "Sorry, install real GNU GPL Free Software GNU/Linux see fsf.org or debian.org"
#endif

#ifdef _MSC_VER
#error "Sorry, install real GNU GPL Free Software GNU/Linux see fsf.org or debian.org"
#endif

#if __APPLE__
#error "Sorry, install real GNU GPL Free Software GNU/Linux see fsf.org or debian.org"
#endif

int main(int argc, char *argv[])
{
    /* read gml or use json_parser.c and jgf json or a modified json_parser.c can read gml data */
    /* update_node_dimension() */
    /* set_layout() */
    /* write as svg image or for GNU/Linux use some small gtk+ code */
    return (0);
}

/* end. */
