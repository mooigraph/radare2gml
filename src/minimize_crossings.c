
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* layer-by-layer sweep */
/* it permutes each layer, trying to find the best ordering for each layer
 * to minimize the number of crossing edges */
void minimize_crossings(const RAGraph * g)
{
    int i = 0;
    int cross_changed = 0;
    int max_changes = MAX_SWEEP_CHANGES;	/* make into a var instead of 4k */
    int rc = 0;			/* rc is number of changes during layer sweep */

    do {
	cross_changed = 0;	/* false */
	max_changes--;

	/* from top of drawing to bottom */
	for (i = 0; i < g->n_layers; i++) {
	    rc = layer_sweep(g->graph, g->layers, g->n_layers, i /* layer number */ , 1 /* true phase-1 */ );
	    /* rc is number of changes */
	    if (rc) {
		cross_changed = 1;
	    } else {
		cross_changed = 0;
	    }
	}
	/* keep on layer sweep while drawing has changes */
    } while (cross_changed && max_changes);

    max_changes = MAX_SWEEP_CHANGES;

    do {
	cross_changed = 0;	/* false */
	max_changes--;

	/* from bottom of drawing to top */
	for (i = g->n_layers - 1; i >= 0; i--) {
	    rc = layer_sweep(g->graph, g->layers, g->n_layers, i /* layer number */ , 0 /* false phase-2 */ );
	    /* rc is number of changes */
	    if (rc) {
		cross_changed = 1;
	    } else {
		cross_changed = 0;
	    }
	}
	/* keep on layer sweep while drawing has changes */
    } while (cross_changed && max_changes);

    /* See the sugiyama theory this needs extra code to improve result more. */

    return;
}

/* end. */
