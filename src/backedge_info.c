
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

void backedge_info(RAGraph * g)
{
    int i = 0;
    int j = 0;
    int k = 0;
    int backedge_info_min = 0;
    int backedge_info_max = 0;	// var rename
    int inedge = 0;
    int outedge = 0;
    int **arr = NULL;

    arr = R_NEWS0(int *, g->n_layers);

    for (i = 0; i < g->n_layers; i++) {
	arr[i] = R_NEWS0(int, 2);
    }

    for (i = 0; i < g->n_layers; i++) {
	for (j = 0; j < g->layers[i].n_nodes; j++) {
	    RGraphNode *gt = g->layers[i].nodes[j];
	    if (!gt) {
		// can this happen?
		continue;
	    }
	    RANode *t = (RANode *) gt->data;
	    if (!t) {
		continue;
	    }
	    // if g->layout ....
	    int tc = g->layout == 0 ? t->x : t->y;
	    int tl = g->layout == 0 ? t->w : t->h;
	    if (!j) {
		arr[i][0] = tc;
		arr[i][1] = tc + tl;
	    }

	    if (arr[i][0] > tc) {
		arr[i][0] = tc;
	    }

	    if (arr[i][1] < tc + tl) {
		arr[i][1] = tc + tl;
	    }
	}

	for (j = 0; j < g->layers[i].n_nodes; j++) {
	    RANode *a = get_anode(g->layers[i].nodes[j]);
	    if (!a || a->is_dummy) {	// split
		continue;
	    }

	    const RList *neighbours = r_graph_get_neighbours(g->graph, a->gnode);
	    RGraphNode *gb;
	    RANode *b;
	    RListIter *itm;

	    if (i == 0) {
		inedge += r_list_length(r_graph_innodes(g->graph, a->gnode));
	    } else if (i == g->n_layers - 1) {
		outedge += r_list_length(neighbours);
	    } else {
		/* this is ? */
	    }

	    graph_foreach_anode(neighbours, itm, gb, b) {
		if (b->layer > a->layer) {
		    continue;
		}

		int nth = count_edges(g, a, b);
		int xinc = R_EDGES_X_INC + 2 * (nth + 1);
// if g->layout == ...
		int ax = g->layout == 0 ? a->x + xinc : a->y + (a->h / 2) + nth;
		int bx = g->layout == 0 ? b->x + xinc : b->y + (b->h / 2) + nth;

		if (g->layout == 0 && nth == 0 && bx > ax) {
		    ax += 4;	/* why +4? */
		}

		backedge_info_min = arr[b->layer][0];
		backedge_info_max = arr[b->layer][1];
		for (k = b->layer; k <= a->layer; k++) {
		    if (backedge_info_min > arr[k][0]) {
			backedge_info_min = arr[k][0];
		    }

		    if (backedge_info_max < arr[k][1]) {
			backedge_info_max = arr[k][1];
		    }
		}

		int l = (ax - backedge_info_min) + (bx - backedge_info_min);
		int r = (backedge_info_max - ax) + (backedge_info_max - bx);

		for (k = b->layer; k <= a->layer; k++) {
		    if (r < l) {
			arr[k][1] = backedge_info_max + 1;
		    } else {
			arr[k][0] = backedge_info_min - 1;
		    }
		}

		AEdge *e = R_NEW0(AEdge);

		e->is_reversed = 1;	/* true */
		e->from = a;
		e->to = b;
		e->x = r_list_new();
		e->y = r_list_new();

		if (r < l) {
		    r_list_append((g->layout == 0 ? e->x : e->y), (void *)(size_t)(backedge_info_max + 1));
		} else {
		    r_list_append((g->layout == 0 ? e->x : e->y), (void *)(size_t)(backedge_info_min - 1));
		}

		r_list_append(g->edges, e);
	    }
	}
    }

    //Assumption: layer layout is not changed w.r.t x-coordinate/y-coordinate for horizontal/vertical layout respectively.
    if (inedge) {
	RANode *n = (RANode *) g->layers[0].nodes[0]->data;
	AEdge *e = R_NEW0(AEdge);

	e->is_reversed = 1;	/* true */
	e->from = NULL;
	e->to = NULL;
	e->x = r_list_new();
	e->y = r_list_new();
	if (g->layout == 0) {
	    r_list_append(e->y, (void *)(size_t)(n->y - 1 - inedge));
	} else {
	    r_list_append(e->x, (void *)(size_t)(n->x - 1 - inedge));
	}
	r_list_append(g->edges, e);
    }

    if (outedge) {
	RANode *n = (RANode *) g->layers[g->n_layers - 1].nodes[0]->data;
	AEdge *e = R_NEW0(AEdge);

	e->is_reversed = 1;	// true;
	e->from = NULL;
	e->to = NULL;
	e->x = r_list_new();
	e->y = r_list_new();
	if (g->layout == 0) {
	    /* vertical layout */
	    r_list_append(e->y, (void *)(size_t)(n->y + g->layers[g->n_layers - 1].height + 2 + outedge));
	} else {
	    /* horizontal layout */
	    r_list_append(e->x, (void *)(size_t)(n->x + g->layers[g->n_layers - 1].width + 2 + outedge));
	}
	r_list_append(g->edges, e);
    }

    for (i = i - 1; i >= 0; i--) {
	R_FREE(arr[i]);
    }

    R_FREE(arr);

    return;
}

/* end. */
