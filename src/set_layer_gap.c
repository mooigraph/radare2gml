
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* see buchheim theory */
void set_layer_gap(RAGraph * g)
{
    int gap = 0;
    int i = 0, j = 0;
    RListIter *itn;
    RGraphNode *ga, *gb;
    RANode *a, *b;
    const RList *outnodes;

    g->layers[0].gap = 0;
    for (i = 0; i < g->n_layers; i++) {
	gap = 0;
	if (i + 1 < g->n_layers) {
	    g->layers[i + 1].gap = gap;
	}
	for (j = 0; j < g->layers[i].n_nodes; j++) {
	    ga = g->layers[i].nodes[j];
	    if (!ga) {		//* ? can happen there are no nodes at this level ?
		continue;
	    }
	    a = (RANode *) ga->data;	// check !a
	    outnodes = ga->out_nodes;

	    if (!outnodes || !a) {
		continue;
	    }
	    graph_foreach_anode(outnodes, itn, gb, b) {
		if (g->layout == 0) {	// vertical layout
		    if ((b->x != a->x) || b->layer <= a->layer) {
			gap += 1;
			if (b->layer <= a->layer) {
			    g->layers[b->layer].gap += 1;
			}
		    } else if ((!a->is_dummy && b->is_dummy) || (a->is_dummy && !b->is_dummy)) {
			gap += 1;
		    } else {
			/* this is ? */
		    }
		} else {	// horizontal layout
		    if ((b->y == a->y && b->h != a->h) || b->y != a->y || b->layer <= a->layer) {
			gap += 1;
			if (b->layer <= a->layer) {
			    g->layers[b->layer].gap += 1;
			}
		    } else if ((!a->is_dummy && b->is_dummy) || (a->is_dummy && !b->is_dummy)) {
			gap += 1;
		    } else {
			/* this is ? */
		    }
		}
	    }
	}
	if (i + 1 < g->n_layers) {
	    g->layers[i + 1].gap += gap;
	}
    }

    return;
}

/* end. */
