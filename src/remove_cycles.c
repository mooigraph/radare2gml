
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

/* find a set of edges that, removed, makes the graph acyclic */
/* invert the edges identified in the previous step to remove the graph cycle */
void remove_cycles(RAGraph * g)
{
    RGraphVisitor cyclic_vis = {
	NULL, NULL, NULL, NULL, NULL, NULL
    };
    const RGraphEdge *e;
    const RListIter *it;

    g->back_edges = r_list_new();
    cyclic_vis.back_edge = (RGraphEdgeCallback) view_cyclic_edge;
    cyclic_vis.data = g;

    /* */
    r_graph_dfs(g->graph, &cyclic_vis);

    r_list_foreach(g->back_edges, it, e) {
	RANode *from = e->from ? get_anode(e->from) : NULL;
	RANode *to = e->to ? get_anode(e->to) : NULL;
	if (from && to) {	// can this happen there is no from/to ?
	    r_agraph_del_edge(g, from, to);
	    r_agraph_add_edge_at(g, to, from, e->nth);
	}
    }

    return;
}

/* end. */
