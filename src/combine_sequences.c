
/*
 *  Copyright radare2 - 2014-2021 - pancake, ret2libc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>

#include "main.h"

/* see the buchheim theory */
void combine_sequences(const RAGraph * g, int l, const RGraphNode * bm, const RGraphNode * bp, int from_up, int a, int r)
{
    RList *Rm = r_list_new();
    RList *Rp = r_list_new();
    const RGraphNode *vt;
    const RGraphNode *vtp;
    RANode *at;
    RANode *atp;
    int rm, rp, t, m, i;

    Rm->free = (RListFree) free;	// use wrapped free()
    Rp->free = (RListFree) free;

    t = ((a + r) / 2);
    vt = g->layers[l].nodes[t - 1];
    vtp = g->layers[l].nodes[t];
    at = get_anode(vt);
    atp = get_anode(vtp);

    collect_changes(g, l, bm, from_up, a, t, Rm, true);
    collect_changes(g, l, bp, from_up, t, r, Rp, false);
    rm = rp = 0;

    m = dist_nodes(g, vt, vtp);
    if (at && atp) {
	while (atp->x - at->x < m) {
	    if (atp->x == at->x) {
		int step = (m / 2);
		at->x -= step;
		atp->x += m - step;
	    } else {
		if (rm < rp) {
		    if (r_list_empty(Rm)) {
			at->x = atp->x - m;
		    } else {
			struct len_pos_t *cx = (struct len_pos_t *)r_list_pop(Rm);
			rm = rm + cx->len;
			at->x = R_MAX(cx->pos, atp->x - m);	// fix this no macro
			R_FREE(cx);
		    }
		} else {
		    if (r_list_empty(Rp)) {
			atp->x = at->x + m;
		    } else {
			struct len_pos_t *cx = (struct len_pos_t *)r_list_pop(Rp);
			rp = rp + cx->len;
			atp->x = R_MIN(cx->pos, at->x + m);	// fix this no macro
			R_FREE(cx);
		    }
		}
	    }
	}
    }

    r_list_free(Rm);
    r_list_free(Rp);

    for (i = (t - 2); i >= a; i--) {
	const RGraphNode *gv = g->layers[l].nodes[i];
	RANode *av = get_anode(gv);
	if (av && at) {
	    av->x = R_MIN(av->x, at->x - dist_nodes(g, gv, vt));	// fix this
	}
    }

    for (i = (t + 1); i < r; i++) {
	const RGraphNode *gv = g->layers[l].nodes[i];
	RANode *av = get_anode(gv);
	if (av && atp) {
	    av->x = R_MAX(av->x, atp->x + dist_nodes(g, vtp, gv));	// fix this
	}
    }

    return;
}

/* end. */
